// web print data custom js
odoo.define('dotmatrixprinter.print_button', function (require) {
    "use strict";

    var form_widget = require('web.form_widgets');
    var core =require('web.core');
    var _t = core._t;
    var QWeb = core.qweb;

    form_widget.WidgetButton.include({

        on_click: function(){
            if(this.node.attrs.custom === "print"){
                console.log("print triggered");
                var url = "http://localhost/var/www/html/dotmatrix/";

                if(this.node.attrs.custom === "print"){
                    url = url + "print.php";
                }

                var view = this.getParent();
                var printer_data = view.datarecord.printer_data;
                if(!printer_data){
                    alert('No data to print. Please click "Update Printer Data"');
                    return;
                }
                // code ajax printer
                console.log(printer_data);
                // urlencode() // jsonp() //json.stringify
                $.ajax({
                    method: "POST",
                    // datatype: "JSON",
                    // crossDomain: true,
                   url: url,
                   data: {
                        printer_data: printer_data
                   },
                   //jsonpCallback: "postHandle",
                   //contentType: "application/jsonp",
                   success: function(data){
                        console.log('Success');
                        console.log('data');
                   },
                   error: function(data){
                         console.log('Failed');
                         console.log('data');
                   },
                });
            }
            else{
                this._super();
            }
        },

    });
});