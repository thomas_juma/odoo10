openerp.oepetstore = function(instance, local) {
    var _t = instance.web._t,
        _lt = instance.web._lt;
    var QWeb = instance.web.qweb;


    local.HomePage = instance.Widget.extend({
    	start: function() {
    		this.$el.append(QWeb.render("HomePageTemplate", {name: "Klaus"}));
    	},
    });

    instance.web.client_actions.add('petstore.homepage', 'instance.oepetstore.HomePage');



    local.NewPage = instance.Widget.extend({
    	start: function() {
    		this.$el.append(QWeb.render("NewPageTemplate", {name: "Santa Klaus"}));
    	},
    });

    instance.web.client_actions.add('petstore.newpage', 'instance.oepetstore.NewPage');

}

